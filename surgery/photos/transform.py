from PIL import Image
import os


crop_area = (352, 0, 2464, 2112)
rotate_angle = 100


# assuming you run a script in a local forlder with images
images = [x for x in os.listdir() if 'jpg' in x.lower()]
images.sort()

for i, img_name in enumerate(images):
    img = Image.open(img_name)

    cropped = img.crop(crop_area)
    rotated = cropped.rotate(rotate_angle)
    rotated.save('%s.jpg' % str(i + 1))
